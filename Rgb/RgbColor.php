<?php
declare(strict_types=1);

/**
 *
 */
class RgbColor
{
    private int $red;
    private int $green;
    private int $blue;

    /**
     * @param int $red
     * @param int $green
     * @param int $blue
     * @throws Exception
     */
    public function __construct(int $red, int $green, int $blue)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
    }

    /**
     * @param int $red
     * @return void
     * @throws Exception
     */
    private function setRed(int $red)
    {
        $this->validate($red);

        $this->red = $red;
    }

    /**
     * @param int $green
     * @return void
     * @throws Exception
     */
    private function setGreen(int $green)
    {
        $this->validate($green);

        $this->green = $green;
    }

    /**
     * @param int $blue
     * @return void
     * @throws Exception
     */
    private function setBlue(int $blue)
    {
        $this->validate($blue);

        $this->blue = $blue;
    }

    /**
     * @return int
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /**
     * @return int
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /**
     * @return int
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /**
     * @param int $value
     * @return void
     * @throws Exception
     */
    private function validate(int $value): void
    {
        if ($value < 0 || $value > 255) {
            throw new Exception('Value less than 0 or more than 255');
        }
    }

    /**
     * @param RgbColor $colour
     * @return bool
     */
    public function equals(RgbColor $colour): bool
    {
        return $colour->getRed() === $colour->getGreen() && $colour->getGreen() === $colour->getBlue();
    }

    /**
     * @param RgbColor $tint
     * @return RgbColor
     * @throws Exception
     */
    public function mix(RgbColor $tint): RgbColor
    {
        return new RgbColor(
            intval(($this->blue + $tint->blue) / 2),
            intval(($this->green + $tint->green) / 2),
            intval(($this->blue + $tint->blue) / 2));
    }

    /**
     * @return RgbColor
     * @throws Exception
     */
    public static function randColor(): RgbColor
    {
        return new RgbColor(rand(0, 255), rand(0, 255), rand(0, 255));
    }
}

$color = RgbColor::randColor();
$color2 = RgbColor::randColor();
$colorRand = $color->mix($color2);
var_dump($colorRand);

//if (!$color->equals($colorRand)) {
//    echo "Colors aren't equals";
//}
