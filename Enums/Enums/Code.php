<?php

namespace Hillell\Enums;

enum Code
{
    case USD;
    case EUR;
    case UAH;
}