<?php
declare(strict_types=1);

namespace Hillel\ValueObjects;

use Hillell\Enums\Code;

/**
 *
 */
class Currency
{
    /**
     * @var Code
     */
    private Code $isoCode;

    /**
     * @param Code $isoCode
     */
    public function __construct(Code $isoCode)
    {
        $this->setIsoCode($isoCode);
    }

    /**
     * @return Code
     */
    public function getIsoCode(): Code
    {
        return $this->isoCode;
    }

    /**
     * @param Code $isoCode
     * @return void
     */
    private function setIsoCode(Code $isoCode): void
    {
        $this->isoCode = $isoCode;
    }

    /**
     * @param Currency $equal
     * @return bool
     */
    public function equals(Currency $equal): bool
    {
        return $this == $equal;
    }
}
