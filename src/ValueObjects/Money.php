<?php
declare(strict_types=1);

namespace Hillel\ValueObjects;

/**
 *
 */
class Money
{
    /**
     * @var int|float
     */
    private int|float $amount;

    /**
     * @var Currency
     */
    private Currency $currency;

    /**
     * @param int|float $amount
     * @param Currency $currency
     * @throws \Exception
     */
    public function __construct(int|float $amount, Currency $currency)
    {
        $this->setAmount($amount);
        $this->setCurrency($currency);
    }

    /**
     * @param Currency $currency
     * @return void
     */
    public function setCurrency(Currency $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return Currency
     */
    public function getCurrency(): Currency
    {
        return $this->currency;
    }

    /**
     * @param int|float $amount
     * @return void
     * @throws \Exception
     */
    private function setAmount(int|float $amount): void
    {
        if ($amount <= 0) {
            throw new \Exception('Invalid amount');
        }

        $this->amount = $amount;
    }


    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param Money $money
     * @return bool
     */
    public function equals(Money $money): bool
    {
        return $this == $money;
    }

    /**
     * @param Money $money
     * @return Money
     * @throws \Exception
     */
    public function add(Money $money): Money
    {
        if ($this->currency != $money->currency) {
            throw new \Exception('Different currencies');
        }

        return new Money($this->getAmount() + $money->getAmount(), $this->getCurrency());
    }
}

