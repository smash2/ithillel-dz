<?php

use Hillel1\Menu\BeefShawarma;
use Hillel1\Menu\LambShawarma;
use Hillel1\Menu\ShawarmaCalculator;
use Hillel1\Menu\ShawarmaOdessa;

require_once 'vendor/autoload.php';

$obj = new BeefShawarma();
$obj1 = new LambShawarma();
$obj2 = new ShawarmaOdessa();

$test = new ShawarmaCalculator();
$test->add($obj1) . '<br>';
$test->add($obj2) . '<br>';
$test->add($obj) . '<br>';
echo '<pre>';
var_dump($test->ingredients());
echo '<pre>';
echo '<pre>';
var_dump($test->price());
echo '<pre>';
echo '<pre>';
var_dump($test->titles());
echo '<pre>';
