<?php

require_once 'vendor/autoload.php';

use Hillell\Enums\Code;
use Hillel\ValueObjects\Currency;
use Hillel\ValueObjects\Money;


$usd = new Currency(Code::EUR);
$usd1 = new Currency(Code::USD);

$user3 = new Money(30, $usd);
$user4 = new Money(30, $usd);

$user = $user3->add($user4);

echo '<pre>';
var_dump($user);
echo '</pre>';
