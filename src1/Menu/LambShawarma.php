<?php

namespace Hillel1\Menu;

class LambShawarma extends Shawarma
{
    protected float $price = 85;
    protected array $ingredients = ['spicy sauce', 'pickled cucumbers', 'cilantro', 'fresh tomatoes', 'pickled onions with barberry and herbs',
        'lamb meat', 'Arabic pita bread'];
    protected string $name = 'Lamb Shawarma';

    public function getCost(): float
    {
        return $this->price;
    }

    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    public function getTitle(): string
    {
        return $this->name;
    }
}