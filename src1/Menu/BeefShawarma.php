<?php

namespace Hillel1\Menu;

class BeefShawarma extends Shawarma
{
    protected float $price = 75;
    protected array $ingredients = ['garlic sauce', 'beef ham', 'pickled cucumbers', 'pickled onions with barberry and herbs',
        'cole slaw salad', 'tandoor lavash', 'fresh tomatoes', 'hummus', 'tahini sauce'];
    protected string $name = 'Beef Shawarma';

    public function getCost(): float
    {
        return $this->price;
    }

    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    public function getTitle(): string
    {
        return $this->name;
    }
}
