<?php

namespace Hillel1\Menu;

class ShawarmaOdessa extends Shawarma
{
    protected float $price = 69;
    protected array $ingredients = ['pickled cucumbers', 'fried potatoes', 'garlic sauce', 'tandoor pita bread',
        'pickled onions with barberry and herbs', 'chicken meat', 'coleslaw salad', 'Korean carrots'];
    protected string $name = 'Shawarma Odessa';

    public function getCost(): float
    {
        return $this->price;
    }

    public function getIngredients(): array
    {
        return $this->ingredients;
    }

    public function getTitle(): string
    {
        return $this->name;
    }
}